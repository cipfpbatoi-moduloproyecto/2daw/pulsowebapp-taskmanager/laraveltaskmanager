<?php
namespace Database\Seeders;
use App\Models\User;
use App\Models\Project;
use App\Models\State;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        DB::table('states')->delete();
        $state= new State;
        $state->id=1;
        $state->name='pendiente';
        $state->save();
        $state= new State;
        $state->id=2;
        $state->name='en proceso';
        $state->save();
        $state= new State;
        $state->id=3;
        $state->name='a supervisar';
        $state->save();
        $state= new State;
        $state->id=4;
        $state->name='realizada';
        $state->save();
    }
}
