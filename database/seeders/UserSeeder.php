<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator;

class UserSeeder extends Seeder
{   
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {   
        DB::table('users')->delete();
        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@gmail.com';
        $admin->password =bcrypt('admin');
        $admin->role = 'admin';
        $admin->save();

        $worker = new User();
        $worker->name = 'worker';
        $worker->email= 'worker@gmail.com';
        $worker->password =bcrypt('worker');
        $worker->role = 'worker';
        $worker->save();

        $worker = new User();
        $worker->name = 'worker';
        $worker->email= 'worker@worker.com';
        $worker->password =bcrypt('worker');
        $worker->role = 'worker';
        $worker->save();

        $customer = new User();
        $customer->name = 'customer';
        $customer->email= 'customer@gmail.com';
        $customer->password =bcrypt('customer');
        $customer->role = 'customer';
        $customer->dni = '213214123A';
        $customer->enterprise = 'customer sl';
        $customer->address = 'customer sl';
        $customer->save();

        $customer = new User();
        $customer->name = 'customer';
        $customer->email= 'customer@customer.com';
        $customer->password =bcrypt('customer');
        $customer->role = 'customer';
        $customer->dni = '213214123A';
        $customer->enterprise = 'customer sl';
        $customer->address = 'customer sl';
        $customer->save();

    }
}
