<?php
namespace Database\Seeders;
use App\Models\User;
use App\Models\Project;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        DB::table('projects')->delete();

        $customers= User::select()->where('role','customer')->get();
        foreach($customers as $customer){
            for ($i=0; $i <1 ; $i++) { 
                $project=new Project();
                $project->customer_id=$customer->id;
                $project->domain='dominio.es';
                $project->active=true;
                $project->delivery_date='2021-04-16 15:15:56';
                $project->save();
            }
        }
    }
}
