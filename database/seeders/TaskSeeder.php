<?php

namespace Database\Seeders;
use App\Models\User;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {                       
        DB::table('tasks')->delete();
        $projects= Project::select()->get();
        $workers= User::select()->where('role','worker')->get();
        foreach($projects as $project){
            foreach($workers as $worker){
                for ($i=0; $i <3 ; $i++) { 
                    $task= new Task();
                    $task->name='task1';
                    $task->description='tasksdescription';
                    $task->state_id=1;
                    $task->used_hours=0;
                    $task->project_id=$project->id;
                    $task->worker_id=$worker->id;
                    $task->save();
                }
            }
        }
    }
}
