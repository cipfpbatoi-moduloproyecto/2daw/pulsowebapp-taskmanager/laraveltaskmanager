<?php
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\Project;
use App\Models\Task;
use App\Models\State;

    function estados($id){
        $state=State::findOrFail($id);
        return $state->name;
    }

    function boolean($bool){
        if(!$bool){
            return 'No';
        }
        else{
            return 'Sí';
        }
    }

?>