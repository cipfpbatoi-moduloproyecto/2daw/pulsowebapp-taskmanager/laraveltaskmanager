<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'state_id',
        'used_hours',
        'project_id',
        'worker_id',
        'timeActive'
    ];
    public function project()
        {
            return $this->belongsTo(Project::class, 'project_id');
        }
    public function worker()
    {
        return $this->belongsTo(User::class, 'worker_id');
    }
    public function hour()
    {
        return $this->hasMany(Hour::class);
    }
}

