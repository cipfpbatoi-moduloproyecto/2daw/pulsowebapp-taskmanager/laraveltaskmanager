<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'play',
        'stop',
        'active',
        'task_id'
    ];

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }
}
