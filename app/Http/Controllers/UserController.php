<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Project;
use App\Models\Task;

use Illuminate\Http\Request;

class UserController extends Controller
{   
    public function checkRole(){
        if(auth()->user()->role=='admin'){      
            return true;
        }
        else{
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     */
    public function index(){
        if(!$this->checkRole()){return redirect()->route('inicio');}   
        $filter='id';
        $users = User::orderBy('id', 'ASC')->paginate(5);
        return view('admin.users.index',['users'=> $users, 'filter'=>$filter,'actualpages'=>5]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!$this->checkRole()){return redirect()->route('inicio');}   
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->checkRole()){return redirect()->route('inicio');}
        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:5|max:20',
            'role' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->role = $request->get('role');
        $user->dni = $request->get('dni');
        $user->enterprise = $request->get('enterprise');
        $user->address = $request->get('address');
        $user->save();
        return back()->with('message', 'User created successfully');    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$this->checkRole()){return redirect()->route('inicio');}   
        $user=User::findOrFail($id);
        return view('admin.users.update', compact('user'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        if(!$this->checkRole()){return redirect()->route('inicio');}   
        $user=User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if(!empty($request->get('password'))){
            $user->password = bcrypt($request->get('password'));
        }
        $user->dni = $request->get('dni');
        $user->enterprise = $request->get('enterprise');
        $user->address = $request->get('address');
        $user->save();
        return back()->with('message', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {           
        if(!$this->checkRole()){return redirect()->route('inicio');}   
        $user=User::findOrFail($id);
        $user->active=false;
        $user->save();
        return back()->with('message', 'User disabled successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {   
        if(!$this->checkRole()){return redirect()->route('inicio');}   
        $user=User::findOrFail($id);
        $user->active=true;
        $user->save();
        return back()->with('message', 'User enabled successfully');
    }

    public function filter($filter,$pages)
    {   
        if(!$this->checkRole()){return redirect()->route('inicio');}   
        $users = User::orderBy($filter, 'ASC')->paginate($pages);
        return view('admin.users.index',['users'=> $users, 'filter'=>$filter,'actualpages'=>$pages]);
    }

    public function getById(Request $request){
        $user=User::findOrFail($request->id);
        return response(json_encode($user),200)->header('Content-type','text-plain');
    }

}
