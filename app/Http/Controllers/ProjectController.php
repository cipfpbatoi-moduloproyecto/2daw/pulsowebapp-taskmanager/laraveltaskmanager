<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Project;
use App\Models\Task;
use App\Models\State;


class ProjectController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        switch (auth()->user()->role) {
            case 'admin':
                $projects = Project::orderBy('id', 'ASC')->paginate(5);
                $customers= User::where('role', 'customer')->get();
                return view('admin.projects.index', compact('projects','customers'));
            break;
            case 'customer':
                $projects= Project::where('customer_id', auth()->user()->id)->get();
                if(count($projects)>1){
                    $hours=array();
                    foreach ($projects as $project) {
                        $hours[$project->id]= Task::where('project_id', $project->id)->sum('used_hours');
                    }          
                    return view('customer.projects.index', compact('projects','hours'));
                }
                else{
                    return redirect()->route('projects.show',$projects[0]->id);
                }
            break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $customers= User::where('role', 'customer')->get();
        return view('admin.projects.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = new Project();
        $project->domain = $request->get('domain');
        $project->customer_id = $request->get('customer_id');
        $project->delivery_date = $request->get('delivery_date');
        $project->save();
        return back()->with('message', 'Project created successfully');    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        $project=Project::findOrFail($id);
        $tasks= Task::where('project_id', $id)->paginate(5);

        switch (auth()->user()->role) {
            case 'admin':
                $workers= User::where('role', 'worker')->get();
                $states= State::get();
                $hours= Task::where('project_id', $id)->sum('used_hours');                
                return view('admin.projects.show', compact('project','tasks','hours','states','workers'));
            break;
            case 'customer':
                $hours= Task::where('project_id', $id)->sum('used_hours');                
                return view('customer.projects.show', compact('project','tasks','hours'));
            break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers= User::where('role', 'customer')->get();
        $project=Project::findOrFail($id);
        return view('admin.projects.update', compact('project','customers'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $project=Project::findOrFail($id);
        $project->domain = $request->get('domain');
        $project->customer_id = $request->get('customer_id');
        $project->delivery_date = $request->get('delivery_date');
        $project->save();
        return back()->with('message', 'Project updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $project=Project::findOrFail($id);
        $project->active=false;
        $project->save();
        return back()->with('message', 'Project disabled successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {   
        $project=Project::findOrFail($id);
        $project->active=true;
        $project->save();
        return back()->with('message', 'Project enabled successfully');
    }

    public function getById(Request $request){
        $project=Project::findOrFail($request->id);
        return response(json_encode($project),200)->header('Content-type','text-plain');
    }
}