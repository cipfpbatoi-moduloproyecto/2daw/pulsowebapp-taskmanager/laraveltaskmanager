<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Project;
use App\Models\Task;
use App\Models\State;
use App\Models\Hour;
use Carbon\Carbon;

use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $states= State::get();

        switch (auth()->user()->role) {
            case 'admin':
                $tasks = Task::orderBy('id', 'ASC')->paginate(5);
                $workers= User::where('role', 'worker')->get();
                $projects= Project::get();
                return view('admin.tasks.index', compact('tasks','workers','states','projects'));
            break;
            case 'worker':
                $tasks = Task::where('worker_id',auth()->user()->id)->orderBy('id', 'ASC')->paginate(5);
                return view('worker.tasks.index', compact('tasks','states'));
            break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $workers= User::where('role', 'worker')->get();
        $states= State::get();
        $projects= Project::get();
        return view('admin.tasks.create', compact('workers','states','projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // $validated = $request->validate([
        //     'name' => 'required|min:2|max:255',
        //     'description' => 'required|min:15',
        //     'state_id' => 'required|numeric|min:1|max:4',
        //     'used_hours' => 'required|numeric|min:0',
        //     'project_id' => 'required|numeric|min:0',
        //     'worker_id' => 'required|numeric|min:0',
        // ]);
        // if ($validator->fails()) {
        //     return back()->withErrors($validator)->withInput();
        // }   
        $task = new Task();
        $task->name = $request->get('name');
        $task->description = $request->get('description');
        $task->state_id = $request->get('state_id');
        $task->used_hours = $request->get('used_hours');
        $task->project_id = $request->get('project_id');
        $task->worker_id = $request->get('worker_id');
        $task->save();
        return back()->with('message', 'Task created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workers= User::where('role', 'worker')->get();
        $states= State::get();
        $projects= Project::get();        
        $task=Task::findOrFail($id);
        return view('admin.tasks.update', compact('task','workers','states','projects'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // $validated = $request->validate([
        //     'name' => 'required|min:2|max:255',
        //     'description' => 'required|min:15',
        //     'state_id' => 'required|numeric|min:1|max:4',
        //     'used_hours' => 'required|numeric|min:0',
        //     'project_id' => 'required|numeric|min:0',
        //     'worker_id' => 'required|numeric|min:0',
        // ]);
        // if ($validator->fails()) {
        //     return back()->withErrors($validator)->withInput();
        // }   
        $task=Task::findOrFail($id);
        $task->name = $request->get('name');
        $task->description = $request->get('description');
        $task->state_id = $request->get('state_id');
        $task->used_hours = $request->get('used_hours');
        $task->project_id = $request->get('project_id');
        $task->worker_id = $request->get('worker_id');
        $task->save();
        return back()->with('message', 'Task updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task=Task::findOrFail($id);
        $task->delete();
        return back()->with('message', 'Task deleted successfully');
    }

    public function getById(Request $request){
        $task=Task::findOrFail($request->id);
        return response(json_encode($task),200)->header('Content-type','text-plain');
    }

    public function state(Request $request){
        $state_id=$request->state_id;
        $task=Task::findOrFail($request->task_id);
        $task->state_id=$state_id;
        $task->save();
        // return response ('true',200)->header('Content-type','text-plain');
        return response ('true',200)->header('Content-type','text-plain');

    }

    public function checkCounter(Request $request){
        $task_id=$request->task_id;
        $hour=Hour::where('task_id', $task_id)->where('active',true)->get();
        if(count($hour)==1){
            return response(json_encode($hour[0]),200)->header('Content-type','text-plain');
        }
        else{
            return response('false',200)->header('Content-type','text-plain');
        }
    }

    public function playHours(Request $request){
        $hour=new Hour();
        $hour->play= Carbon::now();
        $hour->task_id=$request->task_id;
        $hour->active=true;
        $task=Task::findOrFail($hour->task_id);
        $task->state_id=2;
        $task->timeActive=true;
        $task->save();
        if($hour->save()){
            return response(json_encode($hour),200)->header('Content-type','text-plain');
        }
        else{
            return response(false,200)->header('Content-type','text-plain');
        }
    }

    public function stopHours(Request $request){
        $hour=Hour::findOrFail($request->hour_id);
        if(!empty($hour)){     
            $hour->stop= Carbon::now();
            $hour->active=false;
            if($hour->save()){
                $inicio= new Carbon ($hour->play);
                $final= new Carbon ($hour->stop);
                $resultado=$inicio->diffInSeconds($final);
                $task=Task::findOrFail($hour->task_id);
                $used= (int)$task->used_hours;
                $final= $used + $resultado;
                $task->used_hours=$final;
                $task->timeActive=false;
                $task->state_id=1;
                $task->save();
                return response($final,200)->header('Content-type','text-plain');
            }
            else{
                return response(false,200)->header('Content-type','text-plain');
            }
        }
        else{
            return response(false,200)->header('Content-type','text-plain');
        }
    }
}