<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;



class LoginController extends Controller
{
    public function loginForm()
    {
        return view('auth/login');
    }

    public function verificar(Request $request)
	{
			$user= User::where('email', $request->email)->get();
			if(isset($user[0])){
				$user=$user[0];
				$credenciales = $request->only('email', 'password');
				if (Auth::attempt($credenciales))
				{
					return redirect()->route('inicio');
				}
				else {
					return redirect()->route('login')->with('error','Contraseña Incorrecta');
				}
			}
			else {
			return redirect()->route('login')->with('error','Usuario no existe  ');
			}
	}

	public function logout()
	{
		Auth::logout();
        return view('auth/login');
	}


}
// if ($user->email==$request->email && $user->password==$request->password)
// 				{
// 					return redirect()->route('inicio');
// 				}