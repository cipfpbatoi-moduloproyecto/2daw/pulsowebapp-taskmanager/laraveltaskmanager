<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- ===== BOX ICONS ===== -->
        <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Login</title>
    </head>
    <style>
    
    </style>
    <body>
        <div class="login">
            <div class="login__content">
                <div class="login__img">
                    <img src="{{ asset('images/img-login.svg') }}" alt="">
                </div>

                <div class="login__forms">
                    <form class="login__registre" id="login-in" method="POST" action="{{ route('verificar') }}">
                    @csrf
                        <h1 class="login__title">Sign In</h1>
    
                        <div class="login__box">
                            <i class='bx bx-user login__icon'></i>
                            <input type="email" placeholder="Email" name="email" class="login__input" required>
                        </div>
    
                        <div class="login__box">
                            <i class='bx bx-lock-alt login__icon'></i>
                            <input type="password" name="password" placeholder="Password" class="login__input" required>
                        </div>
                        <button type="submit" class="login__button">Sign In</button>
                </div>
            </div>
        </div>
    </body>
</html>