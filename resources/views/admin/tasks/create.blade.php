@extends('plantilla')
@section('titulo', 'Crear Task')
@section('contenido')
<div class="d-flex flex-column justify-content-center align-items-center" style="height: 80%;">
    <h1>Nuevo Task</h1>
	<form action="{{ route('tasks.store') }}" method='POST'>
    @csrf
        <div class="form-group">
            <label for="name">name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="description">description</label>
            <textarea name="description" cols="30" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="state_id">State</label>
            <select name="state_id">
                @foreach($states as $state)
                    <option value="{{$state->id}}">
                        {{$state->name}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="used_hours">used_hours</label>
            <input type="number" name="used_hours" min="0" class="form-control">
        </div>
        <div class="form-group">
            <label for="project_id">Project</label>
            <select name="project_id">
                @foreach($projects as $project)
                    <option value="{{$project->id}}">
                        {{$project->domain}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="worker_id">Worker</label>
            <select name="worker_id">
                @foreach($workers as $worker)
                    <option value="{{$worker->id}}">
                        {{$worker->name}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group text-center"> 
            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">Añadir Task</button>
        </div>
    </form>
</div>
<script>
function showDiv(divId, element){
    document.getElementById(divId).style.display = element.value == 'worker'  ? 'block' : 'none';
}
</script>
@endsection
