@extends('plantilla')
@section('titulo', 'Crear Project')
@section('contenido')
<div class="d-flex flex-column justify-content-center align-items-center" style="height: 80%;">
    <h1>Nuevo Project</h1>
	<form action="{{ route('projects.store') }}" method='POST'>
        @csrf
        <div class="form-group">
            <label for="Domain">Domain</label>
            <input type="text" name="domain" class="form-control">
        </div>
        <label for="customer_id">Customer</label>
        <select name="customer_id">
            @foreach($customers as $customer)
                <option value="{{$customer->id}}">
                    {{$customer->name}}
                </option>
            @endforeach
        </select>
        <div class="form-group">
            <label for="delivery_date">Delivery Date</label>
            <input type="date" name="delivery_date">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">Añadir Project</button>
        </div>
    </form>
</div>
<script>
function showDiv(divId, element){
    document.getElementById(divId).style.display = element.value == 'customer'  ? 'block' : 'none';
}
</script>
@endsection
