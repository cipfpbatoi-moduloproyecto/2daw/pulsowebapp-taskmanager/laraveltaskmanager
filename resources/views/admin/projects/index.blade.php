@extends('plantilla')
@section('titulo', 'Projects')
@section('contenido')
<div class="table-wrapper">
    <div class="table-title">
        <div class="row">
            @if (\Session::has('message'))
                <div class="col-sm-12 alert alert-success alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif 
            @if (count($errors) > 0)
                <div class="col-sm-12 alert alert-warning alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li class="errors-li">@foreach ($errors->all() as $error){{ $error }} <br> @endforeach</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif 
            <div class="col">
                <h2><b>Projects</b></h2>
            </div>
            <div class="col d-flex justify-content-end">
                {{  $projects->links() }}
            </div>                  
        </div>
    </div>
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Id</th>
                <th>Customer</th>
                <th>Domain</th>
                <th>Active</th>
                <th>Delivery Date</th>
				<th></th>
            </tr>
        </thead>
        <tbody>
			@foreach($projects as $project)
            <tr>
                <td>{{  $project->id }}</td>
                <td>{{  $project->customer->name }}</td>
                <td>{{  $project->domain }}</td>
		        <td>{{  boolean($project->active) }}</td>
                <td>{{  $project->delivery_date }}</td>
                <td class="d-flex justify-content-around">
                <a href="{{ route('projects.show', $project->id) }}" class="btn able-button"><i class="bx bx-show-alt"></i></a>
                    <button  user-id="{{$project->id}}" class="btn editProyecto edit-button"><i class='bx bx-pencil'></i></button>
                    @if($project->active==false)
                        <a href="{{  route('projects.activate', $project) }}" class="btn able-button"><i class='bx bx-check'></i></a>
                    @endif
                    @if($project->active==true)
                        <form action="{{  route('projects.destroy', $project) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn unable-button"><i class='bx bx-trash'></i></button>
                        </form>
                    @endif
				</td>
            </tr>
			@endforeach
        </tbody>
    </table>
    <div class="col-12 d-flex justify-content-center">
    <a type="button" class="btn add-button" data-toggle="modal" href="#createProject" ><i class='bx bx-plus'></i></a>
    </div>
</div>
<!-- Add Modal -->
<div class="modal fade" id="createProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Add Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('projects.store') }}" method='POST'>
        @csrf
        <div class="modal-body">
        <div class="form-group">
            <label for="Domain">Domain</label>
            <input type="text" name="domain" class="form-control">
        </div>
        <label for="customer_id">Customer</label>
        <select name="customer_id">
            @foreach($customers as $customer)
                <option value="{{$customer->id}}">
                    {{$customer->name}}
                </option>
            @endforeach
        </select>
        <div class="form-group">
            <label for="delivery_date">Delivery Date</label>
            <input type="date" name="delivery_date">
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn close-button" data-dismiss="modal">Close</button>
        <button type="submit" class="btn add-button" >Add Project</button>
        </div>
    </form>
    </div>
  </div>
</div>
<!-- Edit Modal -->
<div class="modal fade" id="editProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Edit Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('projects.update',1) }}" method='POST'>
        @csrf
        @method('PUT')
        <div class="modal-body">
        <div class="form-group">
            <label for="Domain">Domain</label>
            <input type="text" name="domain" class="form-control">
        </div>
        <label for="customer_id">Customer</label>
        <select name="customer_id">
            @foreach($customers as $customer)
                <option value="{{$customer->id}}">
                    {{$customer->name}}
                </option>
            @endforeach
        </select>
        <div class="form-group">
            <label for="delivery_date">Delivery Date</label>
            <input type="date" name="delivery_date">
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn close-button" data-dismiss="modal">Close</button>
        <button type="submit" class="btn add-button" >Edit Project</button>
        </div>
    </form>
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>

<script>
$(".editProyecto").click(function(){
    $('.modal-dialog').draggable();

    id=$(this).attr('user-id');
    $.ajax({
        url:"{{route('projects.getById')}}",
        method: 'POST',
        data:{
            id:id,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        var project=JSON.parse(res);
        console.log(project);
        $("#editProject").modal('show');
        $("#editProject option[value="+project.customer_id+"]").attr('selected','selected');
        $("#editProject form").attr('action', '/projects/'+ project.id);
        $("#editProject input[name=domain]").val(project.domain);
        $("#editProject input[name=delivery_date]").val(project.delivery_date);
    })
});
</script>
@endsection