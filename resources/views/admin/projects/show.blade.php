@extends('plantilla')
@section('titulo',  " proyecto $project->domain")
@section('contenido')
<div class="table-wrapper">
    <div class="table-title">
        <div class="row">
            @if (\Session::has('message'))
            <div class="col-sm-12 alert alert-success alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif 
            @if (count($errors) > 0)
                <div class="col-sm-12 alert alert-warning alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li class="errors-li">@foreach ($errors->all() as $error){{ $error }} <br> @endforeach</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="col-12 project-card">
                <p><b>ID :</b> {{  $project->id }}</p>
                <p><b>Customer :</b> {{  $project->customer->name }}</p>
                <p><b>Activo :</b> {{  boolean($project->active) }}</p>
                <p><b>Delivery Date :</b> {{  $project->delivery_date }}</p>
                <p><b>Used Time :</b> {{  \Carbon\CarbonInterval::seconds($hours)->cascade() }}</p>
            </div>
            <div class="col">
                <h2><b>{{  $project->domain }}</b></h2>
            </div>
            <div class="col d-flex justify-content-end">
            {{  $tasks->links() }}
            </div>                  
        </div>
    </div>
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Description</th>
                <th>State</th>
                <th>Used Time</th>
                <th>Project</th>
                <th>Worker</th>
				<th></th>
            </tr>
        </thead>
        <tbody>
			@foreach($tasks as $task)
            <tr>
                <td>{{  $task->id }}</td>
                <td>{{  $task->name }}</td>
                <td class="task-description">{{  $task->description }}</td>
				<td>{{  estados($task->state_id) }}</td>
                <td>{{  \Carbon\CarbonInterval::seconds($task->used_hours)->cascade() }}</td>
                <td>{{  $task->project->domain }}</td>
                <td>{{  $task->worker->name }}</td>
                <td class="d-flex justify-content-around">
                    <button  user-id="{{$task->id}}" class="btn editUsuario edit-button editTarea" ><i class='bx bx-pencil'></i></button>
                    <form action="{{  route('tasks.destroy', $task) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn unable-button"><i class='bx bx-trash'></i></button>
                    </form>
				</td>
            </tr>
			@endforeach
        </tbody>
    </table>
    <div class="col-12 d-flex justify-content-center">
        <a type="button" class="btn add-button" data-toggle="modal" href="#createTask" ><i class='bx bx-plus'></i></a>
    </div>
</div>
<!-- Edit Modal -->
<div class="modal fade" id="editTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Edit Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('tasks.store') }}" method='POST'>
        @csrf
        @method('PUT')

        <div class="modal-body">
        <div class="form-group">
            <label for="name">name</label>
            <input type="text" name="name" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="description">description</label>
            <textarea name="description" cols="30" rows="3" required></textarea>
        </div>
        <div class="form-group">
            <label for="state_id">State</label>
            <select name="state_id" required>
                @foreach($states as $state)
                    <option value="{{$state->id}}">
                        {{$state->name}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="used_hours">used_hours</label>
            <input type="number" name="used_hours" min="0" class="form-control" required>
        </div>
            <input type="number" name="project_id" hidden value="{{  $project->id }}">
        <div class="form-group">
            <label for="worker_id">Worker</label>
            <select name="worker_id" required>
                @foreach($workers as $worker)
                    <option value="{{$worker->id}}">
                        {{$worker->name}}
                    </option>
                @endforeach
            </select>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn close-button" data-dismiss="modal">Close</button>
        <button type="submit" class="btn add-button" >Edit Task</button>
        </div>
    </form>
    </div>
  </div>
</div>
<!-- Add Modal -->
<div class="modal fade" id="createTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Add Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('tasks.store') }}" method='POST'>
        @csrf
        <div class="modal-body">
        <div class="form-group">
            <label for="name">name</label>
            <input type="text" name="name" class="form-control" required minlength="2" maxlength="255">
        </div>
        <div class="form-group">
            <label for="description">description</label>
            <textarea name="description" cols="30" rows="3" required minlength="15"></textarea>
        </div>
        <div class="form-group">
            <label for="state_id">State</label>
            <select name="state_id" required>
                @foreach($states as $state)
                    <option value="{{$state->id}}">
                        {{$state->name}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="used_hours">used_hours</label>
            <input type="number" name="used_hours" min="0" class="form-control" value="0" required  min="1">
        </div>
        <input type="number" name="project_id" hidden value="{{  $project->id }}">

        <div class="form-group">
            <label for="worker_id">Worker</label>
            <select name="worker_id" required>
                @foreach($workers as $worker)
                    <option value="{{$worker->id}}">
                        {{$worker->name}}
                    </option>
                @endforeach
            </select>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn close-button" data-dismiss="modal">Close</button>
        <button type="submit" class="btn add-button" >Add Task</button>
        </div>
    </form>
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script>
$(".editTarea").click(function(){
    $('.modal-dialog').draggable();

    id=$(this).attr('user-id');
    $.ajax({
        url:"{{route('tasks.getById')}}",
        method: 'POST',
        data:{
            id:id,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        var task=JSON.parse(res);
        console.log(task);

        $("#editTask").modal('show');
        $("#editTask form").attr('action', '/tasks/'+ task.id);
        $("#editTask input[name=name]").val(task.name);
        $("#editTask textarea[name=description]").val(task.description);

        $("#editTask select[name=state_id] option").removeAttr('selected');
        $("#editTask select[name=state_id] option[value="+task.state_id+"]").attr('selected','selected');

        $("#editTask input[name=used_hours]").val(task.used_hours);

        $("#editTask select[name=project_id] option").removeAttr('selected');
        $("#editTask select[name=project_id] option[value="+task.project_id+"]").attr('selected','selected');

        $("#editTask select[name=worker_id] option").removeAttr('selected');
        $("#editTask select[name=worker_id] option[value="+task.worker_id+"]").attr('selected','selected');

    })
});
</script>
@endsection

