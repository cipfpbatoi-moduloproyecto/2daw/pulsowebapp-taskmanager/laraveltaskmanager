@extends('plantilla')
@section('titulo', 'Editar User')
@section('contenido')
<div class="d-flex flex-column justify-content-center align-items-center" style="height: 80%;">
    <h1>Editar User {{$user->id}}</h1>
	<form action="{{ route('users.update', $user->id) }}" method='POST'>
        @csrf
        @method('PUT')
            <div class="form-group">
                <label for="name">name</label>
                <input type="text" name="name" class="form-control" value="{{$user->name}}">
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="mail" name="email" class="form-control" value="{{$user->email}}">
            </div>
            <div class="form-group">
                <label for="password">password</label>
                <input type="password" name="password" class="form-control" value="{{$user->password}}">
            </div>
            @if($user->role=='customer')            
            <div style="display:block">
            @else
            <div style="display:none">
            @endif
                <div class="form-group">
                    <label for="dni">dni</label>
                    <input type="text" name="dni" class="form-control" value="{{$user->dni}}">
                </div>
                <div class="form-group">
                    <label for="enterprise">enterprise</label>
                    <input type="text" name="enterprise" class="form-control" value="{{$user->enterprise}}">
                </div>
                <div class="form-group">
                    <label for="address">address</label>
                    <input type="text" name="address" class="form-control" value="{{$user->address}}">
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">Editar User</button>
            </div>
    </form>
</div>
@endsection
