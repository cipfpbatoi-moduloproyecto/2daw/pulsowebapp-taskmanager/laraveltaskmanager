@extends('plantilla')
@section('titulo', 'Users')
@section('contenido')
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    @if (\Session::has('message'))
                    <div class="col-sm-12 alert alert-success alert-dismissible fade show" role="alert" style="z-index:99">
                            <ul>
                                <li>{!! \Session::get('message') !!}</li>
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif 
                    @if (count($errors) > 0)
                        <div class="col-sm-12 alert alert-warning alert-dismissible fade show" role="alert" style="z-index:99">
                            <ul>
                                <li class="errors-li">@foreach ($errors->all() as $error){{ $error }} <br> @endforeach</li>
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif 
                    <div class="col">
                        <h2><b>Users</b></h2>
                    </div>
                    
                    <div class="col d-flex justify-content-end">
                    {{  $users->links() }}
                    </div>                  
                </div>
            </div>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>email</th>
                        <th>role</th>
                        <th>dni</th>
                        <th>enterprise</th>
                        <th>address</th>
                        <th>active</th>
						<th></th>
                    </tr>
                </thead>
                <tbody>
					@foreach($users as $user)
                    <tr>
                        <td>{{  $user->id }}</td>
                        <td>{{  $user->name }}</td>
                        <td>{{  $user->email }}</td>
						<td>{{  $user->role }}</td>
                        <td>{{  $user->dni }}</td>
						<td>{{  $user->enterprise }}</td>
						<td>{{  $user->address }}</td>
						<td>{{  boolean($user->active) }}</td>
                        <td class="d-flex justify-content-around">
                        <button  user-id="{{$user->id}}" class="btn editUsuario edit-button" ><i class='bx bx-pencil'></i></button>
                        @if($user->active==false)
                        <a href="{{  route('users.activate', $user) }}" class="btn able-button"><i class='bx bx-check'></i></a>
                        @endif
                        @if($user->active==true)
                        <form action="{{  route('users.destroy', $user) }}" method="POST">
							@method('DELETE')
							@csrf
							<button class="btn unable-button"><i class='bx bx-trash'></i></button>
                        </form>
                        @endif
						</td>
                    </tr>
					@endforeach
					
                </tbody>
            </table>
            <div class="col-12 d-flex justify-content-center">
            <a type="button" class="btn add-button" data-toggle="modal" href="#createUser" ><i class='bx bx-plus'></i></a>
            </div>
        </div>
        
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form  action="{{ route('users.update',1) }}" method='POST'>
        @csrf
        @method('PUT')
        <div class="modal-body">
            <div class="form-group">
                <label for="name">name</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="mail" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">password</label>
                <input type="password" name="password" class="form-control">
            </div>
                <div class="form-group">
                    <label for="dni">dni</label>
                    <input type="text" name="dni" class="form-control">
                </div>
                <div class="form-group">
                    <label for="enterprise">enterprise</label>
                    <input type="text" name="enterprise" class="form-control">
                </div>
                <div class="form-group">
                    <label for="address">address</label>
                    <input type="text" name="address" class="form-control">
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn close-button" data-dismiss="modal">Close</button>
            <button type="submit" class="btn add-button" >Edit User</button>
        </div>
    </form>
    </div>
  </div>
</div>
<!-- Add Modal -->
<div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form  action="{{ route('users.store') }}" method='POST'>
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label for="name">name</label>
                <input type="text" name="name" class="form-control"  minlength="2" maxlength="255" required>
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="mail" name="email" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="password">password</label>
                <input type="password" name="password" class="form-control" minlength="5" maxlength="20" required>
            </div>
            <label for="role">role</label>
            <select name="role" required>
                <option value="admin">admin</option>
                <option value="customer">customer</option>
                <option value="worker" selected>worker</option>
            </select>
                <div class="form-group">
                    <label for="dni">dni</label>
                    <input type="text" name="dni" class="form-control" minlength="7">
                </div>
                <div class="form-group">
                    <label for="enterprise">enterprise</label>
                    <input type="text" name="enterprise" class="form-control" minlength="5">
                </div>
                <div class="form-group">
                    <label for="address">address</label>
                    <input type="text" name="address" class="form-control" minlength="5">
                </div>
            </div>
        <div class="modal-footer">
        <button type="button" class="btn close-button" data-dismiss="modal">Close</button>
        <button type="submit" class="btn add-button" >Add User</button>
        </div>
    </form>
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>

<script>
$(".editUsuario").click(function(){
    $('.modal-dialog').draggable();

    id=$(this).attr('user-id');
    $.ajax({
        url:"{{route('users.getById')}}",
        method: 'POST',
        data:{
            id:id,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        var user=JSON.parse(res);
        console.log(user);
        $("#editUser").modal('show');
        $("#editUser input[name=name]").val(user.name);
        $("#editUser form").attr('action', '/users/'+ user.id);
        $("#editUser input[name=email]").val(user.email);
        $("#editUser input[name=dni]").val(user.dni);
        $("#editUser input[name=enterprise]").val(user.enterprise);
        $("#editUser input[name=address]").val(user.address);
    })
});
</script>
@endsection