@extends('plantilla')
@section('titulo', "Fitxa de User")
@section('contenido')
<div class="d-flex flex-row justify-content-around" style="width:100%;">
	<div class=" card text-white bg-dark" style="width: 18rem;">
		<div class="card-header">Creado en {{  Carbon\Carbon::parse($user->created_at)->format('d/m/Y') }}</div>
		<div class="card-body">
			<h5 class="card-title">Id</h5>
			<p class="card-text">{{ $user->id }}</p>
			<h5 class="card-title">Name</h5>
			<p class="card-text">{{ $user->name }}</p>
			<h5 class="card-title">Email</h5>
			<p class="card-text">{{ $user->email }}</p>
			<h5 class="card-title">Rol</h5>
			<p class="card-text">{{ $user->rol }}</p>
			<a href="{{  route('users.edit', $user) }}" class="btn btn-success" style='height:max-content'>Editar</a>
                <form action="{{  route('users.destroy', $user) }}" method="POST">
					@method('DELETE')
					@csrf
					<button class="btn btn-danger">Borrar</button>
				</form>
		</div>
	</div>
	<div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Órdenes de <b>{{ $user->name }}</b></h2></div>
                    @if (\Session::has('message'))
                        <div class="col-sm-4 alert alert-success">
                            <ul>
                                <li>{!! \Session::get('message') !!}</li>
                            </ul>
                        </div>
                    @endif  
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>State</th>
                        <th>Address</th>
                        <th>User</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
					@foreach($user->orders as $order)
                    <tr>
                        <td>{{  $order->id }}</td>
                        <td>{{  Estado($order->state) }}</td>
                        <td>{{  $order->address }}</td>
						<td>{{  userName($order->user_id) }}</td>
                        <td class="d-flex justify-content-around">
							<a href="{{  route('orders.show', $order) }}" class="btn btn-outline-primary" style='height:max-content'>Ver</a>
							<a href="{{  route('orders_download', $order) }}" class="btn btn-outline-success" style='height:max-content'>Descargar</a>
						</td>
                    </tr>
					@endforeach
                </tbody>
            </table>
        </div>
</div>	
@endsection