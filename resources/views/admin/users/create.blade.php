@extends('plantilla')
@section('titulo', 'Crear User')
@section('contenido')
<div class="d-flex flex-column justify-content-center align-items-center" style="height: 80%;">
    <h1>Nuevo User</h1>
	<form action="{{ route('users.store') }}" method='POST'>
        @csrf
            <div class="form-group">
                <label for="name">name</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="mail" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">password</label>
                <input type="password" name="password" class="form-control">
            </div>
            <label for="role">role</label>
            <select name="role" onchange="showDiv('hidden_div', this)">
                <option value="admin">admin</option>
                <option value="customer">customer</option>
                <option value="worker" selected>worker</option>
            </select>
            <div id="hidden_div" style="display:none">
                <div class="form-group">
                    <label for="dni">dni</label>
                    <input type="text" name="dni" class="form-control">
                </div>
                <div class="form-group">
                    <label for="enterprise">enterprise</label>
                    <input type="text" name="enterprise" class="form-control">
                </div>
                <div class="form-group">
                    <label for="address">address</label>
                    <input type="text" name="address" class="form-control">
                </div>
            </div>
             <div class="form-group text-center">
                <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">Añadir User</button>
            </div>
    </form>
</div>
<script>
function showDiv(divId, element){
    document.getElementById(divId).style.display = element.value == 'customer'  ? 'block' : 'none';
}
</script>
@endsection
