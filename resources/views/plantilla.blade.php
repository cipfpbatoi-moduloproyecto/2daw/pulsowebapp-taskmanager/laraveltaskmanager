<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="dns-prefetch" href="//fonts.gstatic.com">

        <link href="{{ asset('css/boxicons/css/boxicons.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<title>
			@yield('titulo')
		</title>    
	</head>
    <body id="body-pd" class="@yield('titulo')">
        <header class="header" id="header">
            <div class="header__toggle">
                <i class='bx bx-menu' id="header-toggle"></i>
            </div>

            <div class="header__img">
                <img src="{{ asset('images/pulsoweb.svg') }}" alt="">
            </div>
        </header>
        <div class="l-navbar" id="nav-bar">
            <nav class="nav">
                <div>
                    <a href="{{route('inicio')}}" class="nav__logo">
                        <img src="{{ asset('images/pulsoweb-white.svg') }}" alt="" style="height: 25px;">
                        <span class="nav__logo-name">Pulsoweb</span>
                    </a>

                    <div class="nav__list">
                        <!-- <a href="{{route('inicio')}}" class="nav__link">
                            <i class='bx bx-desktop nav__icon' ></i>
                            <span class="nav__name">Desktop</span>
                        </a> -->
                        @if( auth()->user()->role == 'admin')
                        <a href="{{route('users.index')}}" class="nav__link {{ (request()->is('users*')) ? 'active' : '' }}">
                            <i class='bx bx-user nav__icon' ></i>
                            <span class="nav__name">Users</span>
                        </a>
                        @endif
                        @if(auth()->user()->role == 'admin' || auth()->user()->role == 'customer')
                        <a href="{{route('projects.index')}}" class="nav__link {{ (request()->is('projects*')) ? 'active' : '' }}">
                            <i class='bx bx-window-alt nav__icon' ></i>
                            <span class="nav__name">Projects</span>
                        </a>
                        @endif

                        @if(auth()->user()->role == 'admin' || auth()->user()->role == 'worker')
                        <a href="{{route('tasks.index')}}" class="nav__link {{ (request()->is('tasks*')) ? 'active' : '' }}">
                            <i class='bx bx-message-square-check nav__icon' ></i>
                            <span class="nav__name">Tasks</span>
                        </a>
                        @endif

                        <!-- <a href="#" class="nav__link">
                            <i class='bx bx-folder nav__icon' ></i>
                            <span class="nav__name">Data</span>
                        </a>

                        <a href="#" class="nav__link">
                            <i class='bx bx-bar-chart-alt-2 nav__icon' ></i>
                            <span class="nav__name">Analytics</span>
                        </a> -->
                    </div>
                </div>

                <a href="{{route('logout')}}" class="nav__link">
                    <i class='bx bx-log-out nav__icon' ></i>
                    <span class="nav__name">Log Out</span>
                </a>
            </nav>
        </div>
		<div id="contenido">
			@yield('contenido')
		</div>
		</body>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>    

<script src="{{ asset('js/app.js') }}" defer></script>
@yield('javascript')
</html>