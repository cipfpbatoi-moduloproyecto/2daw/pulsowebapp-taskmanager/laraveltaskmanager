@extends('plantilla')
@section('titulo', 'Your Tasks')
@section('contenido')
        @csrf
<div class="table-wrapper">
    <div class="table-title">
        <div class="row">
            @if (\Session::has('message'))
            <div class="col-sm-12 alert alert-success alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif 
            @if (count($errors) > 0)
                <div class="col-sm-12 alert alert-warning alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li class="errors-li">@foreach ($errors->all() as $error){{ $error }} <br> @endforeach</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="col">
                <h2><b>Tasks</b></h2>
            </div>
            
            <div class="col d-flex justify-content-end">
            {{  $tasks->links() }}
            </div>                  
        </div>
    </div>
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Used Time</th>
                <th>Project</th>
                <th>Cron</th>
            </tr>
        </thead>
        <tbody>
			@foreach($tasks as $task)
            <tr class="task_line" task-id="{{$task->id}}">
                <td>{{  $task->name }}</td>
                <td class="task-description">{{  $task->description }}</td>
                <td>{{  \Carbon\CarbonInterval::seconds($task->used_hours)->cascade()}}</td>
                <td>{{  $task->project->domain }}</td>
                <td class="d-flex justify-content-around">
                <div class="crono">
                    <button class="play btn" style="display:block" task-id="{{$task->id}}">
                        <i class="bx bx-play"></i>
                    </button>
                    <button class="pause btn" style="display:none" task-id="{{$task->id}}">
                    <i class="bx bx-pause"></i>
                    </button>
                    <button class="stop btn" style="display:none" task-id="{{$task->id}}">
                    <i class="bx bx-stop"></i>
                    </button>
                </div>
                <div class="taskDone">
                    <button class="check btn able-button" task-id="{{$task->id}}" @if($task->state_id!=1) style="display:none" @endif>
                        <i class="bx bx-check"></i>
                    </button>
                    <button class="uncheck btn unable-button" task-id="{{$task->id}}" @if($task->state_id==1 ) style="display:none" @endif>
                        <i class="bx bx-x"></i>
                    </button>
                </div>
				</td>
            </tr>
			@endforeach
                </tbody>
    </table>
</div>
@endsection
@section('javascript')
<script>
$(document).ready(function(){
	$("tr.task_line").each(function(){
        id=$(this).attr('task-id');
        $.ajax({
        url:"{{route('tasks.check')}}",
        method: 'POST',
        data:{
            task_id:id,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        if(res == 'false'){
            console.log('false');
        }
        else{
            res=JSON.parse(res);
            console.log(res);
            $('button.pause[task-id="'+id+'"]').attr('hour-id',res['id']);
            $('button.stop[task-id="'+id+'"]').attr('hour-id',res['id']);
            $('button.play[task-id="'+id+'"]').css("display", "none");
            $('button.pause[task-id="'+id+'"]').css("display", "block");
            $('button.stop[task-id="'+id+'"]').css("display", "block");
        }
    })

    });
});
$(".play").click(function(){
    id=$(this).attr('task-id');
    $.ajax({
        url:"{{route('tasks.play')}}",
        method: 'POST',
        data:{
            task_id:id,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        var response=JSON.parse(res);
        $('button.pause[task-id="'+id+'"]').attr('hour-id',response['id']);
        $('button.stop[task-id="'+id+'"]').attr('hour-id',response['id']);
        $('button.play[task-id="'+id+'"]').css("display", "none");
        $('button.pause[task-id="'+id+'"]').css("display", "block");
        $('button.stop[task-id="'+id+'"]').css("display", "block");

        var inicio= response['play'];
        console.log(inicio);
    })
});

$(".stop").click(function(){
    task_id=$(this).attr('task-id');
    hour_id=$(this).attr('hour-id');

    $.ajax({
        url:"{{route('tasks.stop')}}",
        method: 'POST',
        data:{
            task_id:task_id,
            hour_id:hour_id,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        console.log(res);
        $('button.play[task-id="'+id+'"]').css("display", "block");
        $('button.pause[task-id="'+id+'"]').css("display", "none");
        $('button.stop[task-id="'+id+'"]').css("display", "none");
    })
});

$(".pause").click(function(){
    task_id=$(this).attr('task-id');
    hour_id=$(this).attr('hour-id');

    $.ajax({
        url:"{{route('tasks.stop')}}",
        method: 'POST',
        data:{
            task_id:task_id,
            hour_id:hour_id,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        console.log(res);
        $('button.play[task-id="'+id+'"]').css("display", "block");
        $('button.pause[task-id="'+id+'"]').css("display", "none");
        $('button.stop[task-id="'+id+'"]').css("display", "none");
    })
});

$(".check").click(function(){
    task_id=$(this).attr('task-id');
    $.ajax({
        url:"{{route('tasks.state')}}",
        method: 'POST',
        data:{
            task_id:task_id,
            state_id:3,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        $('button.check[task-id="'+task_id+'"]').css("display", "none");
        $('button.uncheck[task-id="'+task_id+'"]').css("display", "block");

    })
});

$(".uncheck").click(function(){
    task_id=$(this).attr('task-id');
    $.ajax({
        url:"{{route('tasks.state')}}",
        method: 'POST',
        data:{
            task_id:task_id,
            state_id:1,
            _token:$('input[name="_token"]').val()
        }
    }).done(function(res){
        $('button.check[task-id="'+task_id+'"]').css("display", "block");
        $('button.uncheck[task-id="'+task_id+'"]').css("display", "none");
    })
});
</script>
@endsection