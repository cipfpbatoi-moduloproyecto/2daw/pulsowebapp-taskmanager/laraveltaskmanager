@extends('plantilla')
@section('titulo',  "Project $project->domain")
@section('contenido')
<div class="table-wrapper">
<div class="table-title">
        <div class="row">
            @if (\Session::has('message'))
            <div class="col-sm-12 alert alert-success alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif 
            @if (count($errors) > 0)
                <div class="col-sm-12 alert alert-warning alert-dismissible fade show" role="alert" style="z-index:99">
                    <ul>
                        <li class="errors-li">@foreach ($errors->all() as $error){{ $error }} <br> @endforeach</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="col-12 project-card">
                <p><b>ID :</b> {{  $project->id }}</p>
                <p><b>Customer :</b> {{  $project->customer->name }}</p>
                <p><b>Activo :</b> {{  boolean($project->active) }}</p>
                <p><b>Delivery Date :</b> {{  $project->delivery_date }}</p>
                <p><b>Used Time :</b> {{  \Carbon\CarbonInterval::seconds($hours)->cascade() }}</p>
            </div>
            <div class="col">
                <h2><b>{{  $project->domain }}</b></h2>
            </div>
            <div class="col d-flex justify-content-end">
            {{  $tasks->links() }}
            </div>                  
        </div>
    </div>
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Description</th>
                <th>State</th>
                <th>Used Time</th>
				<th></th>
            </tr>
        </thead>
        <tbody>
			@foreach($tasks as $task)
            <tr>
                <td>{{  $task->id }}</td>
                <td>{{  $task->name }}</td>
                <td class="task-description">{{  $task->description }}</td>
				<td>{{  estados($task->state_id) }}</td>
                <td>{{  \Carbon\CarbonInterval::seconds($task->used_hours)->cascade() }}</td>
                <td class="d-flex justify-content-around">
        
				</td>
            </tr>
			@endforeach
        </tbody>
    </table>
    <div class="col-12 d-flex justify-content-center">
        <a type="button" class="btn add-button" data-toggle="modal" href="#createTask" ><i class='bx bx-plus'></i></a>
    </div>
</div>
        <!-- Add Modal -->
<div class="modal fade" id="createTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Add Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('tasks.store') }}" method='POST'>
        @csrf
        <div class="modal-body">
        <div class="form-group">
            <label for="name">name</label>
            <input type="text" name="name" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="description">description</label>
            <textarea name="description" cols="30" rows="3" required></textarea>
        </div>

        <input type="number" name="state_id" value="1" hidden>
        <input type="number" name="used_hours" min="0"  value="0" hidden>
        <input name="project_id" type="number" hidden value="{{$project->id}}">
        <input name="worker_id" type="number" hidden value="1">

        </div>
        <div class="modal-footer">
        <button type="button" class="btn close-button" data-dismiss="modal">Close</button>
        <button type="submit" class="btn add-button" >Add Task</button>
        </div>
    </form>
    </div>
  </div>
</div>
@endsection