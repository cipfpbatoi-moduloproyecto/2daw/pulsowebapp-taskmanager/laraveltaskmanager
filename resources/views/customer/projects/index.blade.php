@extends('plantilla')
@section('titulo', 'My Projects')
@section('contenido')
			
<div class="d-flex align-items-center justify-content-around w-100 ">
	@foreach($projects as $project)
	<div class="d-flex flex-column project-cards">
		<div class="d-flex align-items-center justify-content-center">
			<h4><b>{{  $project->domain }}</b></h4>
		</div>
		<p><b>ID :</b> {{  $project->id }}</p>
        <p><b>Customer :</b> {{  $project->customer->name }}</p>
        <p><b>Activo :</b> {{  boolean($project->active) }}</p>
        <p><b>Delivery Date :</b> {{  $project->delivery_date }}</p>
        <p><b>Used Time :</b> {{  \Carbon\CarbonInterval::seconds($hours)->cascade() }}</p>
		<div class="d-flex align-items-center justify-content-center">
			<a href="{{ route('projects.show', $project->id) }}" class="btn white-button"><i class="bx bx-show-alt"></i></a>
		</div>
	</div>
	@endforeach
</div>

@endsection