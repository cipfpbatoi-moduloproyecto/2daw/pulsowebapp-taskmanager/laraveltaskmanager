<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')->group( function () {
Route::get('/', function () {
    return view('inicio');
})->name('inicio');

// USUARIOS
Route::resource('users',UserController::Class);
Route::get('users/activate/{id}',  [UserController::class, 'activate'])->name('users.activate');
Route::get('users/filter/{filter}/{pages}',  [UserController::class, 'filter'])->name('users.filter');
Route::post('users/ajax/getById',  [UserController::class, 'getById'])->name('users.getById');

Route::resource('projects',ProjectController::Class);
Route::get('projects/activate/{id}',  [ProjectController::class, 'activate'])->name('projects.activate');
Route::post('projects/ajax/getById',  [ProjectController::class, 'getById'])->name('projects.getById');

Route::resource('tasks',TaskController::Class);
Route::post('tasks/ajax/getById',  [TaskController::class, 'getById'])->name('tasks.getById');
Route::post('tasks/ajax/play',  [TaskController::class, 'playHours'])->name('tasks.play');
Route::post('tasks/ajax/stop',  [TaskController::class, 'stopHours'])->name('tasks.stop');
Route::post('tasks/ajax/check',  [TaskController::class, 'checkCounter'])->name('tasks.check');
Route::post('tasks/ajax/state',  [TaskController::class, 'state'])->name('tasks.state');

});


Route::get('login', [LoginController::class, 'loginForm'])->name('login');
Route::post('login/verify',[LoginController::class, 'verificar'])->name('verificar');
Route::get('/logout',[LoginController::class, 'logout'])->name('logout');
